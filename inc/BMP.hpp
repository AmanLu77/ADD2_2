﻿#pragma once
#include <iostream>
#include <fstream>

#pragma pack(1)
struct BMPHEADER
{
    unsigned short    Type;
    unsigned int      Size;
    unsigned short    Reserved1;
    unsigned short    Reserved2;
    unsigned int      OffBits;
};
#pragma pack()

#pragma pack(1)
struct BMPINFO
{
    unsigned int    Size;
    int             Width;
    int             Height;
    unsigned short  Planes;
    unsigned short  BitCount;
    unsigned int    Compression;
    unsigned int    SizeImage;
    int             XPelsPerMeter;
    int             YPelsPerMeter;
    unsigned int    ClrUsed;
    unsigned int    ClrImportant;
};
#pragma pack()

#pragma pack(1)
struct Pixel
{
    unsigned char b;
    unsigned char g;
    unsigned char r;
};
#pragma pack()

class BMP
{
public:

    BMP();
    BMP(int width, int height);
    BMP(const BMP& bmp);
    BMP& operator=(const BMP& bmp);
    ~BMP();
    void ReadFile(std::string filename);
    void BMP::SaveFile(const std::string& filename);
    void Negative();
    void Encryption();
    void Decryption();

private:
    BMPHEADER m_bmpHeader;
    BMPINFO m_bmpInfo;
    Pixel** m_pixels;
    int m_width, m_height;
};