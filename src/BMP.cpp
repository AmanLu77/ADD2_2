#include <BMP.hpp>

BMP::BMP()
{
    m_width = 0;
    m_height = 0;
}

// �����������
BMP::BMP(int width, int height)
{
    m_width = width;
    m_height = height;
    m_pixels = new Pixel * [m_height];
    for (int i = 0; i < m_height; i++)
        m_pixels[i] = new Pixel[m_width];

    for (int i = 0; i < m_height; i++)
        for (int j = 0; j < m_width; j++)
            m_pixels[i][j] = { 0,0,0 };
}

// ����������� �����������
BMP::BMP(const BMP& bmp)
{
    // �������� ������� ���� �� ����������
    if (m_pixels != nullptr)
    {
        for (int i = 0; i < m_height; i++)
            delete[] m_pixels[i];
        delete[] m_pixels;
    }

    // ������������ ����� ������ � ������
    m_width = bmp.m_width;
    m_height = bmp.m_height;

    // �������� ������ �������
    m_pixels = new Pixel * [m_height];
    for (int i = 0; i < m_height; i++)
        m_pixels[i] = new Pixel[m_width];

    for (int i = 0; i < m_height; i++)
        for (int j = 0; j < m_width; j++)
            m_pixels[i][j] = bmp.m_pixels[i][j];
}

// 2.���������� �������� ������������ ����������� (1 ��� 2)
BMP& BMP::operator=(const BMP& bmp)
{
    // �������� ������� ���� �� ����������
    if (m_pixels != nullptr)
    {
        for (int i = 0; i < m_height; i++)
            delete[] m_pixels[i];
        delete[] m_pixels;
    }

    // ������������ ����� ������ � ������
    m_width = bmp.m_width;
    m_height = bmp.m_height;

    // �������� ������ �������
    m_pixels = new Pixel * [m_height];
    for (int i = 0; i < m_height; i++)
        m_pixels[i] = new Pixel[m_width];

    for (int i = 0; i < m_height; i++)
        for (int j = 0; j < m_width; j++)
            m_pixels[i][j] = bmp.m_pixels[i][j];

    return *this; // ���������� �������������� ���������(���� ��������) �� ������� ��������
}

BMP::~BMP()
{
    for (int i = 0; i < m_height; i++)
        delete[] m_pixels[i];
    delete[] m_pixels;
}

void BMP::ReadFile(std::string filename)
{
    // ������ �����
    std::ifstream in(filename, std::ios::binary); // ������� ���� ��� ��������� ������

    // ������� 14 ������ ��������� � ��������� ��������� BMPHEADER
    in.read(reinterpret_cast<char*>(&m_bmpHeader), sizeof(BMPHEADER));
    in.read(reinterpret_cast<char*>(&m_bmpInfo), sizeof(BMPINFO));

    // ���������� ������ � ������
    m_width = m_bmpInfo.Width;
    m_height = m_bmpInfo.Height;

    // �������� ������ �������
    m_pixels = new Pixel * [m_height];
    for (int i = 0; i < m_height; i++)
        m_pixels[i] = new Pixel[m_width];

    for (int i = 0; i < m_height; i++)
    {
        for (int j = 0; j < m_width; j++)
            in.read(reinterpret_cast<char*>(&m_pixels[i][j]), sizeof(Pixel));

        if ((3 * m_width) % 4 != 0)
            for (int j = 0; j < 4 - (3 * m_width) % 4; j++)
            {
                char c;
                in.read(&c, 1);
            }
    }
}

void BMP::SaveFile(const std::string& filename)
{
    if (m_height == 0 || m_width == 0)
    {
        throw std::exception("Empty file! Zero width or height!");
    }

    // �������� ����
    std::ofstream out(filename, std::ios::binary);

    // ������������ ���������
    BMPHEADER bmpHeader_new;
    bmpHeader_new.Type = 0x4D42; // ��� ������ BMP
    bmpHeader_new.Size = 14 + 40 + (3 * m_width * m_height);
    if (m_width % 4 != 0)
        bmpHeader_new.Size += (4 - (3 * m_width) % 4) * m_height;
    bmpHeader_new.OffBits = 54;
    bmpHeader_new.Reserved1 = 0;
    bmpHeader_new.Reserved2 = 0;

    out.write(reinterpret_cast<char*>(&bmpHeader_new), sizeof(BMPHEADER));

    // ������������ ���������� �� �����������
    BMPINFO bmpInfo_new;
    bmpInfo_new.BitCount = 24;
    bmpInfo_new.ClrImportant = 0;
    bmpInfo_new.ClrUsed = 0;
    bmpInfo_new.Compression = 0;
    bmpInfo_new.Height = m_height;
    bmpInfo_new.Planes = 1;
    bmpInfo_new.Size = 40;
    bmpInfo_new.SizeImage = bmpHeader_new.Size - 54;
    bmpInfo_new.Width = m_width;
    bmpInfo_new.XPelsPerMeter = 0;
    bmpInfo_new.YPelsPerMeter = 0;

    out.write(reinterpret_cast<char*>(&bmpInfo_new), sizeof(BMPINFO));

    // �������� �������
    for (int i = 0; i < bmpInfo_new.Height; i++)
    {
        for (int j = 0; j < bmpInfo_new.Width; j++)
            out.write(reinterpret_cast<char*>(&m_pixels[i][j]), sizeof(Pixel));

        if ((3 * bmpInfo_new.Width) % 4 != 0)
            for (int j = 0; j < 4 - (3 * bmpInfo_new.Width) % 4; j++)
            {
                char c = 0;
                out.write(&c, 1);
            }
    }
}

void BMP::Negative()
{
    for (int i = 0; i < m_bmpInfo.Height; i++)
        for (int j = 0; j < m_bmpInfo.Width; j++)
        {
            m_pixels[i][j].b = 255 - m_pixels[i][j].b;
            m_pixels[i][j].g = 255 - m_pixels[i][j].g;
            m_pixels[i][j].r = 255 - m_pixels[i][j].r;
        }
}
void BMP::Encryption()
{
    for (int i = 0; i < m_bmpInfo.Height; i++)
        for (int j = 0; j < m_bmpInfo.Width; j++)
        {
            if (m_pixels[i][j].b != m_pixels[1][1].b || m_pixels[1][1].g != m_pixels[1][1].g || m_pixels[1][1].r != m_pixels[1][1].r)
            {
                m_pixels[i][j].b = m_pixels[0][0].b;
                m_pixels[i][j].g = m_pixels[0][0].g;
                m_pixels[i][j].r = m_pixels[0][0].r - 1;
            }
        }
    int c = m_pixels[0][0].b;
    //std::cout << "Encription " << c;
}
void BMP::Decryption()
{
    int c = m_pixels[0][0].b;
    //std::cout << "Decipher " << c;
    for (int i = 0; i < m_bmpInfo.Height; i++)
        for (int j = 0; j < m_bmpInfo.Width; j++)
        {
            if (m_pixels[i][j].r != c)
            {
                m_pixels[i][j].b = 0;
                m_pixels[i][j].g = 0;
                m_pixels[i][j].r = 255;
            }
        }
}
