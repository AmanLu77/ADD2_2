﻿#include <iostream>
#include <fstream>
#include <BMP.hpp>

int main()
{
    std::string in = "input.bmp";
    std::string s1 = "encrypted.bmp";
    std::string s2 = "decrypted.bmp";

    try
    {
        // Зашифровка
        BMP newbmp;
        newbmp.ReadFile(in);
        newbmp.Encryption();
        newbmp.SaveFile(s1);

        // Расишифровка
        BMP newbmp2;
        newbmp2.ReadFile(s1);
        newbmp2.Decryption();
        newbmp2.SaveFile(s2);
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
    return 0;
}